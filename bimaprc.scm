;*=====================================================================*/
;*    serrano/prgm/utils/bimap/bimaprc.scm                             */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Sun Mar 27 14:47:23 2005                          */
;*    Last change :  Mon May 22 07:08:45 2006 (serrano)                */
;*    Copyright   :  2005-06 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    An example of BIMAP RC file.                                     */
;*    This should be copied into ~/.config/bimap/bimaprc.scm           */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    Folder filtering                                                 */
;*---------------------------------------------------------------------*/
(bimap-synchronize-folder-predicate-set!
 (lambda (x)
    (or (string-ci=? x "Inbox")
	(string-ci=? x "Inbox.-Unknown")
	(string-ci=? x "Inbox.LaPoste"))))

;*---------------------------------------------------------------------*/
;*    Database filtering                                               */
;*---------------------------------------------------------------------*/
(define bbdb (load-bbdb (make-file-name (getenv "HOME") ".bbdb")))

;*---------------------------------------------------------------------*/
;*    unknown-mail? ...                                                */
;*---------------------------------------------------------------------*/
(define (unknown-mail? s folder msg header)
   (and (string=? folder (bimap-folder-inbox))
	(not (string=? (socket-hostname s) "imap.laposte.net"))
	(bimap-unknown-mail? bbdb s msg header)))
   
;*---------------------------------------------------------------------*/
;*    junk-mail? ...                                                   */
;*---------------------------------------------------------------------*/
(define (junk-mail? header)
   (define (field-is? field val)
      (let ((c (assq field header)))
	 (and (pair? c) (string-ci=? (cdr c) val))))
   (define (get-field field)
      (let ((c (assq field header)))
	 (if (pair? c)
	     (cdr c)
	     "")))
   (let ((to (get-field 'to))
	 (su (get-field 'subject))
	 (fr (get-field 'from)))
      (cond
	 ((field-is? 'x-spam-flag "yes")
	  'spam)
	 ((string=? fr "virusalert@sophia.inria.fr")
	  'virus)
	 ((or (string-contains-ci to "bigloo-owner@sophia.inria.fr")
	      (string-contains-ci to "bigloo-owner@lists-sop.inria.fr"))
	  ;; a mailer daemon
	  (cond
	     ((string-contains-ci fr "mailer-daemon")
	      'bigloo-mailer-daemon)
	     ((or (string-contains-ci su "Returned mail:")
		  (string-contains-ci su "Mail Delivery Sub")
		  (string-contains-ci su "Delivery Status")
		  (string-contains-ci su "Delivery failure")
		  (string-contains-ci su "Delivery Notification")
		  (string-contains-ci su "Undeliverable:")
		  (string-contains-ci su "Benachrichtung"))
	      'bigloo-subject)))
	 ((string-contains-ci su "agos-squash")
	  'agos-squash)
	 ((string-contains-ci su "Cheap Pharmacy")
	  'cheap-pharmacy)
	 (else
	  #f))))

;*---------------------------------------------------------------------*/
;*    bimap-filtering ...                                              */
;*---------------------------------------------------------------------*/
(bimap-filter-set!
 (lambda (s folder msg header)
    (cond
       ((junk-mail? header)
	(bimap-junk-mail! s msg #f))
       ((unknown-mail? s folder msg header)
	(bimap-unknown-mail! s msg))
       (else
	#f))))

;*---------------------------------------------------------------------*/
;*    localhostname ...                                                */
;*---------------------------------------------------------------------*/
(define (localhostname)
   (let loop ((n (hostname)))
      (let ((n2 (prefix n)))
	 (if (string=? n n2)
	     n2
	     (loop n2)))))

;*---------------------------------------------------------------------*/
;*    Localhost vs Inria                                               */
;*---------------------------------------------------------------------*/
(define localhost #unspecified)
(define inria #unspecified)

(bind-exit (exit)
   (with-exception-handler
      (lambda (e)
	 (when (&error? e)
	    (error-notify e))
	 (exit #f))
      (lambda ()
	 (set! localhost
	       (bimap-login (make-ssl-client-socket (localhostname) 993)
			    "serrano"
			    "XXXXXX"))
	 (set! inria
	       (bimap-login (make-ssl-client-socket "imap-sop.inria.fr" 993
						    :timeout 1000000)
			    "serrano"
			    "XXXXXX"))
	 (synchronize! localhost inria))))

;*---------------------------------------------------------------------*/
;*    Machine specific configuration                                   */
;*---------------------------------------------------------------------*/
(let loop ((host (hostname)))
   (when (>fx (string-length host) 0)
      (let ((rc2 (make-file-name (bimap-rc-directory)
				 (format "bimaprc.~a.scm" host))))
	 (cond
	    ((file-exists? rc2)
	     (loadrc rc2))
	    ((>fx (string-length (suffix host)) 0)
	     (loop (prefix host)))))))
