;*=====================================================================*/
;*    serrano/prgm/utils/bimap/share/bimaprc.scm                       */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Sun Mar 27 14:47:23 2005                          */
;*    Last change :  Wed Apr  6 14:31:30 2005 (serrano)                */
;*    Copyright   :  2005 Manuel Serrano                               */
;*    -------------------------------------------------------------    */
;*    BIMAP RC file                                                    */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    Folder filtering                                                 */
;*---------------------------------------------------------------------*/
(bimap-synchronize-folder-predicate-set!
 (lambda (x)
    (and (or (string-ci=? x "Inbox")
	     (string-ci=? x "Inbox.-Unknown")
	     (string-ci=? x "Inbox.Essi")
	     (string-ci=? x "Inbox.LaPoste"))
	 (and (not (substring-ci-at? x "INBOX.Private" 0))
	      (not (string-ci=? x "Inbox.Sent"))
	      (not (string-ci=? x "Inbox.Trash"))
	      (not (string-ci=? x "Inbox.Drafts"))))))

;*---------------------------------------------------------------------*/
;*    Database filtering                                               */
;*---------------------------------------------------------------------*/
(bimap-db-filtering-set!
 (load-bbdb (make-file-name (getenv "HOME") ".bbdb")))

;*---------------------------------------------------------------------*/
;*    Junk filtering                                                   */
;*---------------------------------------------------------------------*/
(bimap-junk-folder-set! #f)

(bimap-junk-predicate-set!
 (lambda (header msg)
    (define (field-is? field val)
       (let ((c (assq field header)))
	  (and (pair? c) (string-ci=? (cdr c) val))))
    (define (get-field field)
       (let ((c (assq field header)))
	  (if (pair? c)
	      (cdr c)
	      "")))
    (let ((to (get-field 'to)))
       (cond
	  ((field-is? 'x-spam-flag "yes")
	   'spam)
	  ((string-contains-ci to "bigloo-owner@sophia.inria.fr")
	   ;; a mailer daemon
	   (let ((fr (get-field 'from))
		 (su (get-field 'subject)))
	      (cond
		 ((string-contains-ci fr "mailer-daemon")
		  'bigloo-mailer-daemon)
		 ((or (string-contains-ci su "Returned mail:")
		      (string-contains-ci su "Mail Delivery Sub")
		      (string-contains-ci su "Delivery Status")
		      (string-contains-ci su "Delivery Notification")
		      (string-contains-ci su "Undeliverable:")
		      (string-contains-ci su "Benachrichtung"))
		  'bigloo-subject))))
	  (else
	   #f)))))

;*---------------------------------------------------------------------*/
;*    Servers settings                                                 */
;*---------------------------------------------------------------------*/
(define localhost
   (bimap-login (make-ssl-client-socket (hostname) 993)
		"XXXXX"
		"YYYYY"))

(define foobar
   (bimap-login (make-ssl-client-socket "foo.bar.fr" 993)
		"XXXXX2"
		"YYYYY2"))

(define laposte
   (bimap-login (make-client-socket "imap.laposte.net" 143)
		"XXXXX3"
		"YYYYY3"))

(synchronize! foobar localhost)
(synchronize! (foobar "INBOX.LaPoste") laposte)
