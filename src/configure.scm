;* Automatically generated file (don't edit) */
;*=====================================================================*/
;*    serrano/prgm/project/bimap2/src/configure.scm.in                 */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Fri Feb 18 17:29:47 2000                          */
;*    Last change :  Sat Dec 25 07:00:01 2004 (serrano)                */
;*    -------------------------------------------------------------    */
;*    BIMAP configuration                                              */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    The module                                                       */
;*---------------------------------------------------------------------*/
(module bimap_configure
   
   (export (bimap-configure ::bstring)
	   (bimap-name)
           (bimap-version)
	   (bimap-bin-directory)
	   (bimap-lib-directory)
	   (bimap-share-directory)
           (bimap-scripts-directory))
   
   (eval   (export bimap-name)
	   (export bimap-version)
	   (export bimap-bin-directory)
	   (export bimap-lib-directory)
	   (export bimap-share-directory)
           (export bimap-scripts-directory)))

;*---------------------------------------------------------------------*/
;*    bimap-configure ...                                              */
;*---------------------------------------------------------------------*/
(define (bimap-configure config)
   (case (string->symbol config)
      ((--version)
       (print (bimap-version)))
      ((--bindir)
       (print (bimap-bin-directory)))
      ((--libdir)
       (print (bimap-lib-directory)))
      ((--sharedir)
       (print (bimap-share-directory)))
      ((--scriptsdir)
       (print (bimap-scripts-directory)))
      (else
       (with-output-to-port (current-error-port)
	  (lambda ()
	     (print "usage: bimap --configure [OPTION]")
	     (newline)
	     (print "Option:")
	     (print "  [--version]")
	     (print "  [--bindir]")
	     (print "  [--libdir]")
	     (print "  [--sharedir]")
             (print "  [--scriptsdir]"))))))
	     
;*---------------------------------------------------------------------*/
;*    Name and version                                                 */
;*---------------------------------------------------------------------*/
(define (bimap-name) "Bimap")
(define (bimap-version) "2.0.2")

;*---------------------------------------------------------------------*/
;*    Install directories ...                                          */
;*---------------------------------------------------------------------*/
(define (bimap-bin-directory) "/usr/local/bin")
(define (bimap-lib-directory) "/usr/local/lib")
(define (bimap-share-directory) "/usr/local/share/bimap")
(define (bimap-scripts-directory) "/usr/local/share/bimap/scripts")

           
