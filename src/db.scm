;*=====================================================================*/
;*    serrano/prgm/utils/bimap/2.0.x/src/db.scm                        */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Wed Mar 30 11:31:16 2005                          */
;*    Last change :  Mon Jun 13 14:10:42 2022 (serrano)                */
;*    Copyright   :  2005-22 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    BIMAP db                                                         */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    The module                                                       */
;*---------------------------------------------------------------------*/
(module bimap_db
   
   (include "mailbox.sch")
   
   (library mail)

   (import  bimap_param
	    bimap_misc)
   
   (eval    (export-all))
   
   (export  (load-bbdb::obj ::bstring)
	    (bbdb-add-entries ::bstring ::pair)
	    (bimap-unknown-mail? ::obj ::mailbox ::pair ::pair-nil)
	    (bimap-unknown-mail! ::mailbox ::pair)))

;*---------------------------------------------------------------------*/
;*    load-bbdb ...                                                    */
;*---------------------------------------------------------------------*/
(define (load-bbdb file)
   
   (define (bbdb9-parse-record rec)
      (match-case rec
	 ((?firstname ?familyname
	     ?- ?- ?org ?phones ?- ?emails ?extras ?uid ?date ?- ?-)
	  (if (pair? emails)
	      (map string-downcase emails)
	      '()))
	 ((?- ?- ?- ?- ?- ?- ?es . ?-)
	  (if (eq? es 'nil) '() (map string-downcase es)))
	 (else
	  '())))

   (define (bbdb6-parse-record rec)
      (match-case rec
	 ((?- ?- ?- ?- ?- ?- ?es . ?-)
	  (if (eq? es 'nil) '() (map string-downcase es)))
	 (else
	  '())))

   (when (file-exists? file)
      (let ((bbdb (make-hashtable)))
	 (with-input-from-file file
	    (lambda ()
	       (let loop ((line (read-line)))
		  (cond
		     ((pregexp-match ";;; file-format: ([0-9]+)" line)
		      =>
		      (lambda (fmt)
			 (for-each (lambda (r)
				      (for-each (lambda (e)
						   (hashtable-put! bbdb e #t))
					 (cond
					    ((string=? (cadr fmt) "6")
					     (bbdb6-parse-record r))
					    ((string=? (cadr fmt) "9")
					     (bbdb9-parse-record r))
					    (else
					     (bbdb9-parse-record r)))))
			    (read))))
		     (else
		      (loop (read-line)))))))
	 bbdb)))

;*---------------------------------------------------------------------*/
;*    bbdb-add-entries ...                                             */
;*---------------------------------------------------------------------*/
(define (bbdb-add-entries file emails)
   (if (not (file-exists? file))
       (with-output-to-file file
	  (lambda ()
	     (print ";;; file-version: 6")
	     (print "user-fields: (www language signature url)")
	     (newline)
	     (print "(")
	     (for-each (lambda (e)
			  (printf "[nil nil nil nil nil nil (~s) nil nil]" e))
		       emails)
	     (newline)
	     (print ")")))
       (let* ((bbdb (load-bbdb file))
	      (lines (with-input-from-file file read-lines)))
	  (with-output-to-file file
	     (lambda ()
		(let loop ((lines lines))
		   (print (car lines))
		   (if (pregexp-match "[(][ \t]*$" (car lines))
		       ;; we got it
		       (begin
			  (for-each (lambda (e)
				       (unless (hashtable-get bbdb e)
					  (printf "[nil nil nil nil nil nil (~s) nil nil]\n" e)))
				    emails)
			  (for-each print (cdr lines)))
		       (loop (cdr lines)))))))))
   
;*---------------------------------------------------------------------*/
;*    bimap-unknown-mail? ...                                          */
;*---------------------------------------------------------------------*/
(define (bimap-unknown-mail? db s msg header)
   
   (define (email-clean nf)
      (when (and (>fx (string-length nf) 0)
		 (char=? (string-ref nf 0) #\<)
		 (char=? (string-ref nf (-fx (string-length nf) 1)) #\>))
	 (substring nf 1 (-fx (string-length nf) 1))))
   
   (and (bimap-db-filter)
	(not (member "\\Seen" (message-info-flags msg)))
	(let ((f (assq 'from header)))
	   (or (not (pair? f))
	       (let ((nf (string-downcase! (email-normalize (cdr f)))))
		  (or (not db)
		      (when (not (hashtable-get db nf))
			 (let ((cf (email-clean nf)))
			    (or (not cf)
				(not (hashtable-get db cf)))))))))))

;*---------------------------------------------------------------------*/
;*    bimap-unknown-mail! ...                                          */
;*---------------------------------------------------------------------*/
(define (bimap-unknown-mail! s msg)
   (let ((unf (string-append (bimap-folder-inbox)
		 (mailbox-separator s)
		 (bimap-folder-unknown))))
      (folder-create! s unf #t)
      (let ((uid (message-info-uid msg))
	    (mid (message-info-mid msg)))
	 (bimap-verb 1
	    "  [" (mail-excerpt 30 s uid) "] "
	    (trace-color 7 "unknowning on ")
	    (mailbox-hostname s)
	    "\n")
	 (bimap-mailbox-message-copy! s uid unf
	    (format "X-Bimap-Unknown-Copy: mailbox=~a\r\n   mid=~a\r\n   flag=~s\r\n   hostname=~a\r\n   date=~a\r\n   from=[~a]\r\n"
	       s mid (mailbox-message-flags s uid) (hostname) (date)
	       (mailbox-message-header-field s uid "From")))
	 (bimap-mailbox-message-delete! s uid
	    (format "X-Bimap-Unknown-Remove: mailbox=~a\r\n   mid=~a\r\n   hostname=~a\r\n   date=~a\r\n" s mid (hostname) (date))
	    unf)
	 #t)))
