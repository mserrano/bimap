;*=====================================================================*/
;*    serrano/prgm/utils/bimap2/src/junk.scm                           */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Thu Mar 31 15:46:10 2005                          */
;*    Last change :  Fri Sep 30 08:53:27 2016 (serrano)                */
;*    Copyright   :  2005-16 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    BIMAP junk filtering                                             */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    The module                                                       */
;*---------------------------------------------------------------------*/
(module bimap_junk

   (include "mailbox.sch")
   
   (library mail)

   (import  bimap_param
	    bimap_misc)
   
   (eval    (export-all))
   
   (export  (bimap-junk-mail! ::mailbox ::pair-nil ::obj)))

;*---------------------------------------------------------------------*/
;*    bimap-junk-mail! ...                                             */
;*---------------------------------------------------------------------*/
(define (bimap-junk-mail! s msg folder)
   (let ((uid (message-info-uid msg)))
      (bimap-log-junk s folder (message-info-mid msg) uid)
      (when (string? folder)
	 (folder-create! s folder #t)
	 (mailbox-message-copy! s uid folder))
      (bimap-mailbox-message-delete! s uid
	 (format "Bimap-Junk: ~a ~a\r\n" folder (date)) folder)
      #t))
   
