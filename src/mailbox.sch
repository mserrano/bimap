;*=====================================================================*/
;*    serrano/prgm/utils/bimap2/src/mailbox.sch                        */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Fri Aug 15 09:37:43 2014                          */
;*    Last change :  Thu Sep 29 19:05:10 2016 (serrano)                */
;*    Copyright   :  2014-16 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    mailbox command wrapper                                          */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    mailbox-message-delete!                                          */
;*---------------------------------------------------------------------*/
(define-expander mailbox-message-delete!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-message-delete!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-message-create!                                          */
;*---------------------------------------------------------------------*/
(define-expander mailbox-message-create!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) (take rest 2)))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-message-create!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-message-copy!                                            */
;*---------------------------------------------------------------------*/
(define-expander mailbox-message-copy!
   (lambda (x e)
      (match-case x
	 ((?fun ?msg ?folder . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,(e msg e) ,folder)
	       (,fun ,(e msg e) ,(e folder e) ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-message-copy!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-message-move!                                            */
;*---------------------------------------------------------------------*/
(define-expander mailbox-message-move!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-message-move!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-message-flags-set! ...                                   */
;*---------------------------------------------------------------------*/
(define-expander mailbox-message-flags-set!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-message-flags-set!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-folder-create! ...                                       */
;*---------------------------------------------------------------------*/
(define-expander mailbox-folder-create!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-folder-create!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-folder-delete! ...                                       */
;*---------------------------------------------------------------------*/
(define-expander mailbox-folder-delete!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-folder-delete!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-folder-delete-messages! ...                              */
;*---------------------------------------------------------------------*/
(define-expander mailbox-folder-delete-messages!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-folder-delete-messages!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-folder-rename! ...                                       */
;*---------------------------------------------------------------------*/
(define-expander mailbox-folder-rename!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-folder-rename!" "wrong form" x)))))

;*---------------------------------------------------------------------*/
;*    mailbox-folder-expunge! ...                                      */
;*---------------------------------------------------------------------*/
(define-expander mailbox-folder-expunge!
   (lambda (x e)
      (match-case x
	 ((?fun . ?rest)
	  `(if (bimap-dry-run)
	       (print ',fun " " ,@(map (lambda (x) (e x e)) rest))
	       (,fun ,@(map (lambda (x) (e x e)) rest))))
	 (else
	  (error "mailbox-folder-expunge!" "wrong form" x)))))

