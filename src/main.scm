;*=====================================================================*/
;*    serrano/prgm/utils/bimap/2.0.x/src/main.scm                      */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Sun Mar 27 08:38:36 2005                          */
;*    Last change :  Thu Aug 19 17:12:53 2021 (serrano)                */
;*    Copyright   :  2005-21 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    IMAP synchronization                                             */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    The module                                                       */
;*---------------------------------------------------------------------*/
(module bimap
   
   (library mail ssl)
   
   (import  bimap_param
	    bimap_misc
	    bimap_sync
	    bimap_db
	    bimap_configure)

   (eval    (export loadrc))

   (main    main))

;*---------------------------------------------------------------------*/
;*    main ...                                                         */
;*---------------------------------------------------------------------*/
(define (main args)
   ;; default tracing and verbing
   (bimap-verbose-set! 1)
   (bigloo-debug-set! 0)
   ;; command line parsing
   (parse-args args)
   ;; add entries
   (when (pair? (bimap-db-add-entries))
      (bbdb-add-entries (bimap-bbdb-file) (bimap-db-add-entries)))
   ;; synchronization
   (unwind-protect
      (when (bimap-synchronize)
	 (synchronize-all!))
      ;; logout
      (bimap-logout)))

;*---------------------------------------------------------------------*/
;*    parse-args ...                                                   */
;*---------------------------------------------------------------------*/
(define (parse-args args)

   (define (usage proc)
      (print "bimap-" (bimap-version) " [ options ]")
      (newline)
      (proc #f))
   
   (let ((loadp #t)
	 (replp #f)
	 (rc (make-file-name (bimap-rc-directory) (bimap-rc-file)))
	 (ignore #f))
      (args-parse (cdr args)
	 ((("-h" "--help") (help "This message"))
	  (usage args-parse-usage)
	  (exit 0))
	 (("--version" (help "Display the version number and exit"))
	  (print (bimap-name) " v" (bimap-version))
	  (exit 0))
	 (("--options" (help "Display the options and exit"))
	  (usage args-parse-usage)
	  (exit 0))
	 (("-q" (help "Do not load an init file"))
	  (set! loadp #f))
	 (("-v?level" (help "Increase or set verbosity level (-v0 crystal silence)"))
	  (if (string=? level "")
	      (bimap-verbose-set! (+fx 1 (bimap-verbose)))
	      (bimap-verbose-set! (string->integer level))))
	 (("-g?level" (help "Increase or set debug level"))
	  (if (string=? level "")
	      (bigloo-debug-set! (+fx 1 (bigloo-debug)))
	      (bigloo-debug-set! (string->integer level))))
	 (("--log" ?file (help "Loggin activity file"))
	  (bimap-log-file-set! file))
	 (("--no-color" (help "Disable coloring"))
	  (bigloo-trace-color-set! #f))
	 (("--rc-file" ?file (help (format "RC file name (default ~s)" rc)))
	  (set! rc file))
	 (("--repl" (help "Start a repl"))
	  (set! replp #t))
	 (("--synchronization" (help "Enable synchronization (default)"))
	  (bimap-synchronize-set! #t))
	 (("--no-synchronization" (help "Disable synchronization"))
	  (bimap-synchronize-set! #f))
	 (("--db-filtering" (help "Enable db-filtering (default)"))
	  (bimap-db-filter-set! #t))
	 (("--no-db-filtering" (help "Disable db-filtering"))
	  (bimap-db-filter-set! #f))
	 (("--db-add-entry" ?email (help "Add an entry for the db-filtering"))
	  (bimap-db-add-entries-set! (cons email (bimap-db-add-entries))))
	 (("--force-lock" (help "Force lock aquisition"))
	  (bimap-force-lock-set! #t))
	 ((("-t" "--lock-timeout") ?tmt (help "Lock timeout"))
	  (bimap-lock-timeout-set! (string->integer tmt)))
	 ((("-n" "--dry-run") (help "Print the commands that would be executed"))
	  (bimap-dry-run-set! #t))
	 (("--configure" ?config (help "Report BIMAP configuration"))
	  (bimap-configure config)
	  (exit 0))
	 (("--" (help "Ignore remaining options"))
	  (set! ignore #t))
	 (else
	  (unless ignore
	     (usage args-parse-usage)
	     (exit 1))))
      (when loadp (loadrc rc))
      (when replp (repl) (exit 0))))

;*---------------------------------------------------------------------*/
;*    loadrc ...                                                       */
;*---------------------------------------------------------------------*/
(define (loadrc file)
   (if (string? file)
       (if (file-exists? file)
	   (begin
	      (bimap-verb 2 "Loading `" file "'...\n")
	      (eval '(library-load 'mail))
	      (eval '(library-load 'ssl))
	      (loadq file))
	   (warning 'bimap "Can't find rc file `" file "', ignoring...\n"))))

