;*=====================================================================*/
;*    serrano/prgm/utils/bimap/2.0.x/src/misc.scm                      */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Wed Mar 30 11:49:18 2005                          */
;*    Last change :  Sun Jul  3 15:16:54 2022 (serrano)                */
;*    Copyright   :  2005-22 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    BIMAP misc                                                       */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    The module                                                       */
;*---------------------------------------------------------------------*/
(module bimap_misc
   
   (include "mailbox.sch")
   
   (library ssl mail)
   
   (eval    (export make-ssl-client-socket)
            (export-exports))
   
   (import  bimap_param
	    bimap_sync)
   
   (export  (bimap-verb ::int . ::obj)

	    (bimap-log-copy ::mailbox ::bstring ::bstring ::mailbox ::bstring ::int)
	    (bimap-log-delete ::mailbox ::bstring ::bstring ::int)
	    (bimap-log-update ::mailbox ::bstring ::bstring ::pair-nil ::int)
	    (bimap-log-junk ::mailbox ::bstring ::bstring ::int)


	    (bimap-sync-info-folder s1::mailbox f1::bstring s2::mailbox f2::bstring)
	    (bimap-sync-info-load s::mailbox syncf::bstring)
	    (bimap-sync-info-store! s::mailbox synct syncf::bstring mid uid flags)
	    
	    (folder-exists?::bool ::mailbox ::bstring)
	    (folder-parent ::bstring ::char)
	    (folder-create! ::mailbox ::bstring ::bool)
	    (folder-delete! ::mailbox ::bstring)
	    (folder-length::int ::mailbox ::bstring)
	    
	    (mail-excerpt ::int ::mailbox ::int)
	    (message-copy! ::mailbox ::bstring ::bstring ::obj)
	    (inline message-info-uid msg)
	    (inline message-info-mid msg)
	    (inline message-info-flags msg)
	    (inline message-info-flags-set! msg flags)
	    
	    (filter-flags::obj ::obj)
	    
	    (bimap-mailbox-message-flags s uid table)
	    (bimap-mailbox-message-header s uid)
	    
	    (bimap-login::mailbox ::obj ::bstring #!optional passwd alias)
	    (bimap-get-login ::mailbox)
	    (bimap-lock! ::mailbox ::mailbox)
	    (bimap-unlock! ::mailbox ::mailbox)
	    (bimap-logout)
	    (bimap-folder ::mailbox)

	    (make-mailbox-folder-name ::mailbox ::bstring)
	    (make-folder-name ::bstring ::bstring ::bstring)
	    
	    (bimap-mailbox-folder-name ::mailbox ::bstring)
	    (bimap-sync-folder-name-set! ::mailbox ::bstring)
	    (bimap-mailbox-message-create! m::mailbox f::bstring s::bstring)
	    (bimap-mailbox-message-delete! ::mailbox ::obj ::obj #!optional folder)
	    (bimap-mailbox-message-copy! ::mailbox ::int ::bstring #!optional extra)
	    (bimap-mailbox-message-exists? ::mailbox ::int ::bstring)))

;*---------------------------------------------------------------------*/
;*    object-print ::mailbox ...                                       */
;*---------------------------------------------------------------------*/
(define-method (object-print obj::mailbox port print-slot)
   (display "#<" port)
   (display (typeof obj) port)
   (display "@" port)
   (display (mailbox-hostname obj) port)
   (display ">" port))

;*---------------------------------------------------------------------*/
;*    object-print ::imap ...                                          */
;*---------------------------------------------------------------------*/
(define-method (object-print obj::imap port print-slot)
   (call-next-method))

;*---------------------------------------------------------------------*/
;*    object-print ::maildir ...                                       */
;*---------------------------------------------------------------------*/
(define-method (object-print obj::maildir port print-slot)
   (call-next-method))

;*---------------------------------------------------------------------*/
;*    bimap-verb ...                                                   */
;*---------------------------------------------------------------------*/
(define (bimap-verb lvl . obj)
   (when (>=fx (bimap-verbose) lvl)
      (for-each display obj)
      (flush-output-port (current-output-port))))

;*---------------------------------------------------------------------*/
;*    bimap-log ...                                                    */
;*---------------------------------------------------------------------*/
(define (bimap-log action::symbol mb::mailbox folder::bstring mid::bstring)
   (when (string? (bimap-log-file))
      (let ((l (format "[~a] ~a ~a@~a [~a]\n"
		  mid action folder (mailbox-hostname mb) (date))))
	 (call-with-append-file (bimap-log-file)
	    (lambda (op) (display l op))))))

;*---------------------------------------------------------------------*/
;*    bimap-log-copy ...                                               */
;*---------------------------------------------------------------------*/
(define (bimap-log-copy s1 f1 mid s2 f2 uid)
   (bimap-log 'COPY s2 f2 mid)
   (bimap-verb 1 "  [" (mail-excerpt 30 s1 uid) "] "
      (trace-color 5 "copied to ") s2 "\n"))

;*---------------------------------------------------------------------*/
;*    bimap-log-delete ...                                             */
;*---------------------------------------------------------------------*/
(define (bimap-log-delete s1 f1 mid uid)
   (bimap-log 'DELETE s1 f1 mid)
   (bimap-verb 1 "  [" (mail-excerpt 30 s1 uid) "] "
      (trace-color 6 "deleted on ") s1 "\n"))

;*---------------------------------------------------------------------*/
;*    bimap-log-update ...                                             */
;*---------------------------------------------------------------------*/
(define (bimap-log-update s1 f1 mid flags uid)
   (bimap-log 'UPDATE s1 f1 mid)
   (bimap-verb 1 "  [" (mail-excerpt 30 s1 uid) "] "
      (trace-color 5 "updated on ") s1 " -> " flags "\n"))

;*---------------------------------------------------------------------*/
;*    bimap-log-junk ...                                               */
;*---------------------------------------------------------------------*/
(define (bimap-log-junk s1 f1 mid uid)
   (bimap-log 'JUNK s1 f1 mid)
   (bimap-verb 1 "  [" (mail-excerpt 30 s1 uid) "] "
      (trace-color 8 "junked on ") s1 "\n"))

;*---------------------------------------------------------------------*/
;*    imap? ...                                                        */
;*---------------------------------------------------------------------*/
(define (imap? o) (isa? o imap))

;*---------------------------------------------------------------------*/
;*    folder-exists? ...                                               */
;*---------------------------------------------------------------------*/
(define (folder-exists? s folder)
   (any (lambda (f) (string-ci=? folder f)) (mailbox-folders s)))

;*---------------------------------------------------------------------*/
;*    folder-parent ...                                                */
;*---------------------------------------------------------------------*/
(define (folder-parent folder isep)
   (let ((len (string-length folder)))
      (let loop ((i (-fx len 1)))
	 (cond
	    ((=fx i 0)
	     #f)
	    ((char=? (string-ref folder i) isep)
	     (substring folder 0 i))
	    (else
	     (loop (-fx i 1)))))))

;*---------------------------------------------------------------------*/
;*    folder-create! ...                                               */
;*    -------------------------------------------------------------    */
;*    Creates a folder. Create the parent first if it does not         */
;*    exists.                                                          */
;*---------------------------------------------------------------------*/
(define (folder-create! s folder::bstring verb::bool)
   (let ((folders (mailbox-folders s))
	 (isep (string-ref (mailbox-separator s) 0)))
      (let loop ((folder folder))
	 (let ((parent (folder-parent folder isep)))
	    (if (and parent (not (folder-exists? s parent)))
		(begin
		   (loop parent)
		   (mailbox-folder-create! s parent)))))
      (unless (folder-exists? s folder)
	 (when verb
	    (bimap-verb 1 "  " (mailbox-hostname s) "://" folder
	       "..." (trace-color 5 "created") "\n"))
	 (mailbox-folder-create! s folder))))

;*---------------------------------------------------------------------*/
;*    folder-delete! ...                                               */
;*---------------------------------------------------------------------*/
(define (folder-delete! s folder)
   (bimap-verb 1 "  " (mailbox-hostname s) "://" folder
      "..." (trace-color 6 "deleted") "\n")
   (mailbox-folder-select! s folder)
   (for-each (lambda (i)
		(bimap-mailbox-message-delete! s (car i)
		   (format "X-Bimap-Remove: folder \"~a\" deleted ~a\r\n" folder (date))))
      (mailbox-folder-infos s))
   (mailbox-folder-expunge! s)
   (mailbox-folder-select! s (folder-parent folder (string-ref (mailbox-separator s) 0)))
   (mailbox-folder-delete! s folder))

;*---------------------------------------------------------------------*/
;*    folder-status-next-uid ...                                       */
;*---------------------------------------------------------------------*/
(define (folder-status-next-uid::int status::pair-nil)
   (let ((c (assq 'uidnext status)))
      (if (pair? c)
	  (cdr c)
	  0)))

;*---------------------------------------------------------------------*/
;*    message-info-mid ...                                             */
;*---------------------------------------------------------------------*/
(define-inline (message-info-mid msg)
   (let ((c (assq 'message-id (cdr msg))))
      (when (and (pair? c) (string? (cdr c)))
	 (cdr c))))

;*---------------------------------------------------------------------*/
;*    message-info-flags ...                                           */
;*---------------------------------------------------------------------*/
(define-inline (message-info-flags msg)
   (let ((c (assq 'flags (cdr msg))))
      (if (pair? c)
	  (cdr c)
	  '())))

;*---------------------------------------------------------------------*/
;*    message-info-flags-set! ...                                      */
;*---------------------------------------------------------------------*/
(define-inline (message-info-flags-set! msg flags)
   (let ((c (assq 'flags (cdr msg))))
      (when (pair? c)
	 (set-cdr! c flags))))

;*---------------------------------------------------------------------*/
;*    message-info-uid ...                                             */
;*---------------------------------------------------------------------*/
(define-inline (message-info-uid msg)
   (car msg))

;*---------------------------------------------------------------------*/
;*    folder-length ...                                                */
;*---------------------------------------------------------------------*/
(define (folder-length s folder)
   (let* ((st (mailbox-folder-status s folder))
	  (c (assq 'messages st)))
      (if (pair? c)
	  (cdr c)
	  0)))
	     
;*---------------------------------------------------------------------*/
;*    excerpt ...                                                      */
;*---------------------------------------------------------------------*/
(define (excerpt len str)
   (if (<fx (string-length str) len)
       str
       (substring str 0 len)))

;*---------------------------------------------------------------------*/
;*    mail-excerpt ...                                                 */
;*---------------------------------------------------------------------*/
(define (mail-excerpt len s uid)
   (string-append
    (excerpt 5 (mailbox-message-header-field s uid "Message-ID"))
    ":"
    (excerpt (/fx len 2) (mailbox-message-header-field s uid "From"))
    ", "
    (excerpt (/fx len 2) (mailbox-message-header-field s uid "Subject"))))
    
;*---------------------------------------------------------------------*/
;*    message-copy! ...                                                */
;*    -------------------------------------------------------------    */
;*    Copy MSG to server S into folder F and returns the internal      */
;*    date of this new message.                                        */
;*---------------------------------------------------------------------*/
(define (message-copy! s f content flags)
   (with-handler
      (lambda (e)
	 (error-notify e)
	 #f)
      (let ((tf (bimap-mailbox-folder-name s f)))
	 (mailbox-folder-select! s tf)
	 (let* ((uid (folder-status-next-uid (mailbox-folder-status s tf)))
		(nuid (bimap-mailbox-message-create! s tf content)))
	    (mailbox-folder-poll s)
	    (when (pair? flags)
	       (mailbox-message-flags-set! s uid flags))
	    #t))))

;*---------------------------------------------------------------------*/
;*    filter-flags ...                                                 */
;*---------------------------------------------------------------------*/
(define (filter-flags flags)
   (if (pair? flags)
       (if (pair? flags)
	   (filter (lambda (x) (not (string-ci=? x "\\Recent"))) flags)
	   flags)
       flags))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-flags ...                                  */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-flags s uid table)
   (let ((intable (assoc uid table)))
      (filter-flags
	 (if (pair? intable) (cdr intable) (mailbox-message-flags s uid)))))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-header ...                                 */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-header s uid)
   (with-handler
      (lambda (e) '())
      (mail-header->list (mailbox-message-header s uid))))

;*---------------------------------------------------------------------*/
;*    *logins* ...                                                     */
;*---------------------------------------------------------------------*/
(define *logins* '())

;*---------------------------------------------------------------------*/
;*    purify-login-name ...                                            */
;*---------------------------------------------------------------------*/
(define (purify-login-name str)
   (string-replace! (string-replace str #\space #\-) #\. #\_))

;*---------------------------------------------------------------------*/
;*    bimap-login ...                                                  */
;*---------------------------------------------------------------------*/
(define (bimap-login obj login #!optional passwd alias)
   (let ((conn (if (socket? obj)
		   (if (not passwd)
		       (error 'bimap-login
			      "password required for imap"
			      #unspecified)
		       (instantiate::imap
			  (socket (imap-login obj login passwd))))
		   (if passwd
		       (error 'bimap-login
			      "password forbiddedn for maildir"
			      #unspecified)
		       (instantiate::maildir
			  (path obj)))))
	 (name (purify-login-name (or alias login))))
      (set! *logins* (cons (cons conn name) *logins*))
      conn))

;*---------------------------------------------------------------------*/
;*    bimap-get-login ...                                              */
;*---------------------------------------------------------------------*/
(define (bimap-get-login m::mailbox)
   (let ((c (assoc m *logins*)))
      (if (not c)
	  (error 'bimap "Can't find login for socket" m)
	  (cdr c))))

;*---------------------------------------------------------------------*/
;*    mailbox-message-create/w-dry! ...                                */
;*---------------------------------------------------------------------*/
(define (mailbox-message-create/w-dry! . args)
   (apply mailbox-message-create! args))

;*---------------------------------------------------------------------*/
;*    mailbox-message-delete/w-dry! ...                                */
;*---------------------------------------------------------------------*/
(define (mailbox-message-delete/w-dry! . args)
   (apply mailbox-message-delete! args))

;*---------------------------------------------------------------------*/
;*    mailbox-folder-expunge/w-dry! ...                                */
;*---------------------------------------------------------------------*/
(define (mailbox-folder-expunge/w-dry! . args)
   (apply mailbox-folder-expunge! args))

;*---------------------------------------------------------------------*/
;*    bimap-lock! ...                                                  */
;*---------------------------------------------------------------------*/
(define (bimap-lock! s1 s2)
   
   (define (lock! s1 s2)
      (let ((msg (format "Subject: bimap \r\nX-Date: #e~a\r\n\r\nlock:~a\r\n\r\n"
		    (date->seconds (current-date))
		    (mailbox-hostname s2))))
	 (mailbox-message-create/w-dry! s1 (bimap-folder s1) msg)))
   
   (define (unlocked? s)
      (and s (=fx (folder-length s (bimap-folder s)) 0)))
   
   (define (try-unlock! s)
      (let ((uids (mailbox-folder-uids s)))
	 (when (pair? uids)
	    (let ((uid (car uids)))
	       (let ((lock (mailbox-message-header-field s uid "X-Date")))
		  (when (string? lock)
		     (let ((ldate (with-input-from-string lock read))
			   (now (date->seconds (current-date))))
			(when (and (elong? ldate)
				   (>elong (-elong now ldate) #e120))
			   ;; release the lock
			   (mailbox-message-delete/w-dry! s uid)
			   (mailbox-folder-expunge/w-dry! s)))))))))
   
   (bimap-verb 2 " locking "
      (trace-color 4 (mailbox-hostname s1))
      " and "
      (trace-color 4 (mailbox-hostname s2)) "...")

   (folder-create! s1 (bimap-folder s1) #f)
   (folder-create! s2 (bimap-folder s2) #f)
   (with-handler
      (lambda (e)
	 (error "bimap" "Cannot select folder"
	    (format "~a@a" (bimap-folder s1) s1)))
      (mailbox-folder-select! s1 (bimap-folder s1)))
   (with-handler
      (lambda (e)
	 (error "bimap" "Cannot select folder"
	    (format "~a@a" (bimap-folder s2) s2)))
      (mailbox-folder-select! s2 (bimap-folder s2)))
   (let loop ((l1 s1)
	      (l2 s2)
	      (ttl (bimap-lock-timeout)))
      (cond
	 ((and (not l1) (not l2))
	  ;; the lock are acquired
	  (bimap-verb 2 "ok.\n")
	  #unspecified)
	 ((=fx ttl 0)
	  (bimap-verb 2 (trace-color 1 "error") "\n")
	  (when l1
	     (bimap-verb 2 " Can't acquire locks for "
		(trace-color 3 (mailbox-hostname s1))))
	  (when l2
	     (bimap-verb 2 " Can't acquire locks for "
		(trace-color 3 (mailbox-hostname s2))))
	  (bimap-verb 2 " (see --force-lock)...exiting.\n")
	  (unless l1 (force-unlock! s1))
	  (unless l2 (force-unlock! s2))
	  (exit 1))
	 ((and l1 (unlocked? l1))
	  ;; aquire the lock on l1
	  (lock! l1 s2)
	  (loop #f l2 ttl))
	 ((and l2 (unlocked? l2))
	  (lock! l2 s1)
	  (loop l1 #f ttl))
	 (else
	  (sleep 1000000)
	  (when l1 (try-unlock! l1))
	  (when l2 (try-unlock! l2))
	  (loop l1 l2 (-fx ttl 1))))))

;*---------------------------------------------------------------------*/
;*    force-unlock! ...                                                */
;*---------------------------------------------------------------------*/
(define (force-unlock! s)
   (when (mailbox-folder-exists? s (bimap-folder s))
      (bimap-verb 2 " unlocking " (trace-color 4 (mailbox-hostname s)))
      (mailbox-folder-select! s (bimap-folder s))
      (for-each (lambda (i)
		   (mailbox-message-delete/w-dry! s (message-info-uid i)))
	 (mailbox-folder-infos s))
      (mailbox-folder-expunge/w-dry! s)
      (bimap-verb 2 "...ok.\n")))

;*---------------------------------------------------------------------*/
;*    bimap-unlock! ...                                                */
;*---------------------------------------------------------------------*/
(define (bimap-unlock! s1 s2)
   (unwind-protect
      (force-unlock! s1)
      (force-unlock! s2)))

;*---------------------------------------------------------------------*/
;*    bimap-logout ...                                                 */
;*---------------------------------------------------------------------*/
(define (bimap-logout)
   (for-each (lambda (v)
		(let ((m1 (vector-ref v 0))
		      (f1 (vector-ref v 1))
		      (m2 (vector-ref v 2))
		      (f2 (vector-ref v 3)))
		   (bimap-verb 2 " disconnecting "
		      (trace-color 3 m1 "://" f1) " <-> " (trace-color 4 m2 "://" f2)
		      "\n")
		   (when (imap? m1)
		      (with-access::imap m1 ((s1 socket))
			 (unless (socket-down? s1)
			    (imap-logout s1)
			    (socket-shutdown s1))))
		   (when (imap? m2)
		      (with-access::imap m2 ((s2 socket)) 
			 (unless (socket-down? s2)
			    (imap-logout s2)
			    (socket-shutdown s2))))))
      *synchronizations*)
   (bimap-verb 1 "logout.\n"))

;*---------------------------------------------------------------------*/
;*    bimap-folder ...                                                 */
;*    -------------------------------------------------------------    */
;*    Returns the bimap folder name for mailbox S.                     */
;*---------------------------------------------------------------------*/
(define (bimap-folder s)
   (let ((o (hashtable-get (bimap-folder-name-table) s)))
      (if (string? o)
	  o
	  (let ((n (string-append (bimap-folder-inbox)
				  (mailbox-separator s)
				  (bimap-folder-name))))
	     (hashtable-put! (bimap-folder-name-table) s n)
	     n))))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-folder-name ...                                    */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-folder-name s::mailbox folder::bstring)
   (let ((sep (mailbox-separator s)))
      (cond
	 ((string=? sep ".") (string-replace folder #\/ #\.))
	 ((string=? sep "/") (string-replace folder #\. #\/))
	 (else folder))))

;*---------------------------------------------------------------------*/
;*    bimap-sync-info-folder ...                                       */
;*    -------------------------------------------------------------    */
;*    Returns the folder name for the sync-info of f1@s1 vs f2@s2.     */
;*---------------------------------------------------------------------*/
(define (bimap-sync-info-folder s1::mailbox f1::bstring s2::mailbox f2::bstring)

   (define (folder-name->string str)
      (string-replace (string-replace str #\/ #\_) #\. #\_))
   
   (string-append (bimap-folder s1)
      (mailbox-separator s1)
      ;; host names
      (folder-name->string (mailbox-hostname s1))
      "_"
      (folder-name->string (mailbox-hostname s2))
      (mailbox-separator s1)
      ;; folder names
      (folder-name->string f1)
      "_"
      (folder-name->string f2)))

;*---------------------------------------------------------------------*/
;*    bimap-sync-info-load ...                                         */
;*    -------------------------------------------------------------    */
;*    Load into a hashtable all the messages from a sync-info folder.  */
;*---------------------------------------------------------------------*/
(define (bimap-sync-info-load s::mailbox syncf::bstring)
   (let ((synct (make-hashtable)))
      (when (mailbox-folder-exists? s syncf)
	 (mailbox-folder-select! s syncf)
	 (for-each (lambda (m1)
		      (let* ((mid (message-info-mid m1))
			     (uid (message-info-uid m1))
			     (flags (message-info-flags m1)))
			 (when mid
			    (hashtable-put! synct mid (cons flags uid)))))
	    (mailbox-folder-infos s)))
      synct))

;*---------------------------------------------------------------------*/
;*    bimap-sync-info-store! ...                                       */
;*---------------------------------------------------------------------*/
(define (bimap-sync-info-store! s::mailbox synct syncf::bstring mid uid flags)
   (unless (mailbox-folder-exists? s syncf)
      (folder-create! s syncf #f))
   (mailbox-folder-select! s syncf)
   (let* ((content (string-append "Message-ID: " mid "\r\n"))
	  (nuid (bimap-mailbox-message-create! s syncf content)))
      (when (integer? nuid)
	 (mailbox-message-flags-set! s nuid flags)
	 (hashtable-put! synct mid (cons flags nuid))
	 (when uid (mailbox-message-delete! s uid))
	 (mailbox-folder-expunge! s))))
   
;*---------------------------------------------------------------------*/
;*    make-mailbox-folder-name ...                                     */
;*---------------------------------------------------------------------*/
(define (make-mailbox-folder-name mb::mailbox folder::bstring)
   (make-folder-name folder "." (mailbox-separator mb)))

;*---------------------------------------------------------------------*/
;*    make-folder-name ...                                             */
;*---------------------------------------------------------------------*/
(define (make-folder-name f sepfrom::bstring septo::bstring)
   (if (string=? sepfrom septo)
       f
       (string-replace f (string-ref sepfrom 0) (string-ref septo 0))))

;*---------------------------------------------------------------------*/
;*    bimap-sync-folder-name-set! ...                                  */
;*---------------------------------------------------------------------*/
(define (bimap-sync-folder-name-set! s n)
   (hashtable-put! (bimap-folder-name-table) s n))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-create! ...                                */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-create! mbox folder msg)
   (let ((uid (folder-status-next-uid (mailbox-folder-status mbox folder))))
      (mailbox-message-create! mbox folder msg)
      uid))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-delete! ...                                */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-delete! mbox uid reason #!optional folder)
   (when (string? (bimap-backup-directory))
      (bimap-mailbox-message-plain-backup
	 (bimap-backup-directory) folder mbox uid))
   (when (and (isa? (bimap-backup-mailbox) mailbox)
	      (not (eq? (bimap-backup-mailbox) mbox)))
      (bimap-mailbox-message-mailbox-backup
	 (bimap-backup-mailbox) (bimap-backup-mailbox-folder) folder mbox uid
	 reason))
   (mailbox-message-delete! mbox uid))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-plain-backup ...                           */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-plain-backup dir folder mbox uid)
   
   (define (header-get-field header field)
      (let ((c (assq field header)))
	 (when (pair? c)
	    (cdr c))))
   
   (let* ((header (with-handler
		     (lambda (e)
			(exception-notify e)
			'())
		     (mailbox-message-header-list mbox uid)))
	  (hid (header-get-field header 'message-id))
	  (hfrom (header-get-field header 'from))
	  (hto (header-get-field header 'to))
	  (hsubject (header-get-field header 'subject))
	  (hdate (header-get-field header 'date))
	  (flags (mailbox-message-flags mbox uid))
	  (name (string-replace! (format "~a:~a.~a" hid hfrom hdate)
		   (file-separator) #\-))
	  (repo (if (string? folder)
		    (make-file-path dir folder)
		    dir)))
      (make-directories repo)
      ;; backup the message
      (with-output-to-file (make-file-path repo name)
	 (lambda ()
	    (display (mailbox-message-header mbox uid))
	    (display (mailbox-message-body mbox uid))))
      ;; add an entry in the deleted list
      (let ((p (append-output-file (make-file-name dir "MESSAGES"))))
	 (fprintf p "~a ~a\n  from=~a to=~a\n  subject=~a\n\n" hdate hid hfrom hto hsubject)
	 (close-output-port p))))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-mailbox-backup ...                         */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-mailbox-backup bckbox bckfolder folder mbox uid reason)
   
   (define (folder-basename folder sep)
      (let ((i (string-index-right folder (string-ref sep 0))))
	 (if i (substring folder (+fx i 1)) folder)))
   
   (let* ((pref (string-append
		   (mailbox-prefix bckbox)
		   (mailbox-separator bckbox)))
	  (folder (if (string? folder)
		      (string-append
			 pref bckfolder (mailbox-separator bckbox)
			 (folder-basename  folder (mailbox-separator mbox)))
		      (string-append pref bckfolder)))
	  (content (mailbox-message mbox uid))
	  (content+ (if (string? reason)
			(string-append reason content)
			content))
	  (flags (mailbox-message-flags mbox uid)))
      (unless (mailbox-folder-exists? bckbox folder)
	 (mailbox-folder-create! bckbox folder))
      (message-copy! bckbox folder content+ flags)))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-copy! ...                                  */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-copy! m::mailbox uid::int f::bstring
	   #!optional extra)
   (if extra
       (mailbox-message-create! m f
	  (string-append extra (mailbox-message m uid)))
       (mailbox-message-copy! m uid f)))

;*---------------------------------------------------------------------*/
;*    bimap-mailbox-message-exists? ...                                */
;*---------------------------------------------------------------------*/
(define (bimap-mailbox-message-exists? m::mailbox uid::int f::bstring)
   (when (mailbox-folder-exists? m f)
      (mailbox-folder-select! m f)
      (memq uid (mailbox-folder-uids m))))

