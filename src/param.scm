;*=====================================================================*/
;*    serrano/prgm/utils/bimap2/src/param.scm                          */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Wed Mar 30 11:24:45 2005                          */
;*    Last change :  Wed Nov  2 20:18:49 2016 (serrano)                */
;*    Copyright   :  2005-16 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    BIMAP parameters                                                 */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    The module                                                       */
;*---------------------------------------------------------------------*/
(module bimap_param
   
   (eval   (export-all))
   
   (export (bimap-verbose::int)
	   (bimap-verbose-set! ::int)
	   
	   (bimap-db-add-entries::pair-nil)
	   (bimap-db-add-entries-set! ::pair-nil)
	   
	   (bimap-synchronize::bool)
	   (bimap-synchronize-set! ::bool)
	   
	   (bimap-db-filter::bool)
	   (bimap-db-filter-set! ::bool)
	   
	   (bimap-rc-directory::bstring)
	   (bimap-rc-directory-set! ::bstring)
	   
	   (bimap-rc-file::bstring)
	   (bimap-rc-file-set! ::bstring)
	   
	   (bimap-folder-name::bstring)
	   (bimap-folder-name-set! ::bstring)
	   
	   (bimap-folder-unknown::bstring)
	   (bimap-folder-unknown-set! ::bstring)
	   
	   (bimap-folder-inbox::bstring)
	   (bimap-folder-inbox-set! ::bstring)
	   
	   (bimap-folder-name-table)
	   
	   (bimap-bbdb-file::obj)
	   (bimap-bbdb-file-set! ::obj)
	   
	   (bimap-filter::procedure)
	   (bimap-filter-set! ::procedure)
	   
	   (bimap-folder-rewrite::procedure)
	   (bimap-folder-rewrite-set! ::procedure)
	   
	   (bimap-synchronize-folder-predicate::procedure)
	   (bimap-synchronize-folder-predicate-set! ::procedure)

	   (bimap-delete-folders::bool)
	   (bimap-delete-folders-set! ::bool)
	   
	   (bimap-force-lock::bool)
	   (bimap-force-lock-set! ::bool)
	   
	   (bimap-backup-directory::obj)
	   (bimap-backup-directory-set! ::obj)
	   
	   (bimap-backup-mailbox::obj)
	   (bimap-backup-mailbox-set! ::obj)
	   
	   (bimap-backup-mailbox-folder::obj)
	   (bimap-backup-mailbox-folder-set! ::obj)
	   
	   (bimap-lock-timeout::int)
	   (bimap-lock-timeout-set! ::int)

	   (bimap-dry-run::bool)
	   (bimap-dry-run-set! ::bool)

	   (bimap-log-file::obj)
	   (bimap-log-file-set! ::obj)))

;*---------------------------------------------------------------------*/
;*    define-parameter ...                                             */
;*---------------------------------------------------------------------*/
(define-macro (define-parameter id default . setter)
   (let ((vid (symbol-append '* id '*)))
      `(begin
          (define ,vid ,default)
          (define (,id)
             ,vid)
          (define (,(symbol-append id '-set!) v)
             ,(if (pair? setter)
                  `(set! ,vid (,(car setter) v))
                  `(set! ,vid v))
             v))))

;*---------------------------------------------------------------------*/
;*    bimap-verbose ...                                                */
;*---------------------------------------------------------------------*/
(define-parameter bimap-verbose 1)

;*---------------------------------------------------------------------*/
;*    bimap-db-add-entries ...                                         */
;*---------------------------------------------------------------------*/
(define-parameter bimap-db-add-entries '())

;*---------------------------------------------------------------------*/
;*    bimap actions                                                    */
;*---------------------------------------------------------------------*/
(define-parameter bimap-synchronize #t)
(define-parameter bimap-db-filter #t)

;*---------------------------------------------------------------------*/
;*    bimap-rc-directory ...                                           */
;*---------------------------------------------------------------------*/
(define-parameter bimap-rc-directory
   (let ((home (getenv "HOME"))
	 (host (hostname)))
      (let loop ((host (if (not (string? host)) (getenv "HOST") host)))
	 (if (string? host)
	     (let ((home/host (string-append home "/.config/bimap" host)))
		(if (and (file-exists? home/host) (directory? home/host))
		    home/host
		    (if (string=? (suffix host) "")
			(let ((home/def (make-file-name home ".config/bimap")))
			   (cond
			      ((and (file-exists? home/def)
				    (directory? home/def))
			       home/def)
			      (else
			       home)))
			(loop (prefix host)))))))))

;*---------------------------------------------------------------------*/
;*    bimap-rc-file ...                                                */
;*---------------------------------------------------------------------*/
(define-parameter bimap-rc-file "bimaprc.scm")

;*---------------------------------------------------------------------*/
;*    bimap folders ...                                                */
;*---------------------------------------------------------------------*/
(define-parameter bimap-folder-inbox "INBOX")
(define-parameter bimap-folder-unknown "-Unknown")
(define-parameter bimap-folder-name "bimap")

;*---------------------------------------------------------------------*/
;*    bimap-folder-name-table ...                                      */
;*---------------------------------------------------------------------*/
(define-parameter bimap-folder-name-table (make-hashtable 32))

;*---------------------------------------------------------------------*/
;*    bimap-bbdb-file ...                                              */
;*---------------------------------------------------------------------*/
(define-parameter bimap-bbdb-file
   (make-file-name (getenv "HOME") ".bbdb"))

;*---------------------------------------------------------------------*/
;*    bimap-synchronize-folder-predicate ...                           */
;*---------------------------------------------------------------------*/
(define-parameter bimap-synchronize-folder-predicate (lambda (x) x))

;*---------------------------------------------------------------------*/
;*    bimap-delete-folders ...                                         */
;*---------------------------------------------------------------------*/
(define-parameter bimap-delete-folders #f)

;*---------------------------------------------------------------------*/
;*    bimap-filter ...                                                 */
;*---------------------------------------------------------------------*/
(define-parameter bimap-filter (lambda (s fd msg hd) #f))

;*---------------------------------------------------------------------*/
;*    bimap-folder-rewrite ...                                         */
;*---------------------------------------------------------------------*/
(define-parameter bimap-folder-rewrite (lambda (s fd msg hd) fd))

;*---------------------------------------------------------------------*/
;*    bimap-backup-directory ...                                       */
;*---------------------------------------------------------------------*/
(define-parameter bimap-backup-directory #f)
(define-parameter bimap-backup-mailbox #f)
(define-parameter bimap-backup-mailbox-folder #f)

;*---------------------------------------------------------------------*/
;*    locks ...                                                        */
;*---------------------------------------------------------------------*/
(define-parameter bimap-force-lock #f)
(define-parameter bimap-lock-timeout 10)

;*---------------------------------------------------------------------*/
;*    dry-run ...                                                      */
;*---------------------------------------------------------------------*/
(define-parameter bimap-dry-run #f)


;*---------------------------------------------------------------------*/
;*    bimap-log-file ...                                               */
;*---------------------------------------------------------------------*/
(define-parameter bimap-log-file #f)
