;*=====================================================================*/
;*    serrano/prgm/utils/bimap/2.0.x/src/sync.scm                      */
;*    -------------------------------------------------------------    */
;*    Author      :  Manuel Serrano                                    */
;*    Creation    :  Wed Mar 30 11:48:20 2005                          */
;*    Last change :  Sun Jul  3 15:21:54 2022 (serrano)                */
;*    Copyright   :  2005-22 Manuel Serrano                            */
;*    -------------------------------------------------------------    */
;*    BIMAP synchronization                                            */
;*=====================================================================*/

;*---------------------------------------------------------------------*/
;*    The module                                                       */
;*---------------------------------------------------------------------*/
(module bimap_sync
   
   (include "mailbox.sch")
   
   (library mail)
   
   (import  bimap_param
	    bimap_db
	    bimap_junk
	    bimap_misc)
   
   (eval    (export register-synchronize!)
	    (export synchronize-servers-folders!)
	    (export synchronize-servers!))
   
   (export  (synchronize-all!)
	    *synchronizations*))

;*---------------------------------------------------------------------*/
;*    mailbox? ...                                                     */
;*---------------------------------------------------------------------*/
(define (mailbox? o) (isa? o mailbox))
		
;*---------------------------------------------------------------------*/
;*    synchronize! ...                                                 */
;*---------------------------------------------------------------------*/
(eval 
 '(define-macro (synchronize! s1 s2)
     (list 'register-synchronize!
	   (if (pair? s1) (cons 'list s1) s1)
	   (if (pair? s2) (cons 'list s2) s2))))

;*---------------------------------------------------------------------*/
;*    synchronize-folder? ...                                          */
;*---------------------------------------------------------------------*/
(define (synchronize-folder? s x root)
   (and (not (substring-at? x (bimap-folder s) 0))
	(substring-ci-at? x root 0)
	(let ((f (make-folder-name x "/" ".")))
	   ((bimap-synchronize-folder-predicate) f))))

;*---------------------------------------------------------------------*/
;*    mailbox-folder-mids-table ...                                    */
;*    -------------------------------------------------------------    */
;*    Returns a hashtable mapping MIDs to UIDs.                        */
;*---------------------------------------------------------------------*/
(define (mailbox-folder-mids-table s::mailbox folder::bstring)
   (let* ((lst (mailbox-folder-infos s))
	  (len (max (length lst) 10))
	  (table (make-hashtable (*fx 3 len))))
      (for-each (lambda (m)
		   (let ((mid (message-info-mid m)))
		      (when (string? mid)
			 (hashtable-put! table mid (message-info-uid m)))))
	 lst)
      table))
      
;*---------------------------------------------------------------------*/
;*    synchronize-folders! ...                                         */
;*---------------------------------------------------------------------*/
(define (synchronize-folders! 
	   s1::mailbox f1::bstring
	   s2::mailbox f2::bstring)

   (define (flags-equal? l1 l2)
      (when (=fx (length l1) (length l2))
	 (not (find (lambda (x) (not (member x l2))) l1))))
   
   (define (synchronize-messages-insync
	      syncs::mailbox synct::struct syncf::bstring
	      s1::mailbox f1::bstring
	      s2::mailbox f2::bstring)
      ;; (tprint "synchronize-messages-insync")
      (mailbox-folder-select! s1 f1)
      (mailbox-folder-select! s2 f2)
      
      (let ((mids1 (mailbox-folder-mids-table s1 f1))
	    (mids2 (mailbox-folder-mids-table s2 f2))
	    (tf1 (mailbox-folder-flags s1))
	    (tf2 (mailbox-folder-flags s2)))
	 (hashtable-for-each synct
	    (lambda (mid::bstring synci::pair)
	       (let ((uid1 (hashtable-get mids1 mid))
		     (uid2 (hashtable-get mids2 mid))
		     (flagsy (car synci))
		     (uidy (cdr synci)))
		  (cond
		     ((and uid1 uid2)
		      (let ((flags1 (bimap-mailbox-message-flags s1 uid1 tf1))
			    (flags2 (bimap-mailbox-message-flags s2 uid2 tf2)))
			 (cond
			    ((flags-equal? flags1 flags2)
			     ;; m1.flags == m2.flags
			     (unless (equal? flags1 flagsy)
				(bimap-sync-info-store! syncs synct syncf
				   mid uidy flags1)
				(mailbox-folder-select! s1 f1)))
			    ((flags-equal? flags1 flagsy)
			     ;; m1.flags outsync
			     (bimap-log-update s1 f1 mid flags2 uid1)
			     (mailbox-message-flags-set! s1 uid1 flags2)
			     (bimap-sync-info-store! syncs synct syncf
				mid uidy flags2)
			     (mailbox-folder-select! s1 f1))
			    ((flags-equal? flags2 flagsy)
			     ;; m2.flags outsync
			     (bimap-log-update s2 f2 mid flags1 uid2)
			     (mailbox-message-flags-set! s2 uid2 flags1)
			     (bimap-sync-info-store! syncs synct syncf
				mid uidy flags1)
			     (mailbox-folder-select! s1 f1)))))
		     (uid1
		      ;;; still present in s1, removed from s2
		      (bimap-log-delete s1 f1 mid uid1)
		      (mailbox-message-delete! s1 uid1)
		      (mailbox-folder-select! s1 syncf)
		      (mailbox-message-delete! s1 uidy)
		      (mailbox-folder-select! s1 f1))
		     (uid2
		      ;;; still present in s2, removed from s1
		      (bimap-log-delete s2 f2 mid uid2)
		      (mailbox-message-delete! s2 uid2)
		      (mailbox-folder-select! s1 syncf)
		      (mailbox-message-delete! s1 uidy)
		      (mailbox-folder-select! s1 f1))
		     (else
		      ;;; removed from both s1 and s2
		      (mailbox-folder-select! s1 syncf)
		      (mailbox-message-delete! s1 uidy)
		      (mailbox-folder-select! s1 f1))))))))
   
   (define (synchronize-messages-outsync
	      syncs::mailbox synct::struct syncf::bstring
	      s1::mailbox f1::bstring
	      s2::mailbox f2::bstring)
      ;; (tprint "synchronize-messages-outsync")
      (mailbox-folder-select! s1 f1)
      (mailbox-folder-select! s2 f2)
      (let ((mids1 (mailbox-folder-mids-table s1 f1))
	    (mids2 (mailbox-folder-mids-table s2 f2))
	    (tf1 (mailbox-folder-flags s1))
	    (tf2 (mailbox-folder-flags s2)))
	 (hashtable-for-each mids1
	    (lambda (mid1 uid1)
	       (unless (hashtable-get synct mid1)
		  (let ((uid2 (hashtable-get mids2 mid1)))
		     (when uid2
			(let ((flags1 (bimap-mailbox-message-flags s1 uid1 tf1))
			      (flags2 (bimap-mailbox-message-flags s2 uid2 tf2)))
			   (cond
			      ((equal? flags1 flags2)
			       ;; m1.flags == m2.flags
			       (bimap-sync-info-store! syncs synct syncf
				  mid1 #f flags1)
			       (mailbox-folder-select! s1 f1))
			      (else
			       ;; two possible options, arbitrary
			       ;; propagate flags1
			       (bimap-log-update s2 f2 mid1 flags1 uid2)
			       (mailbox-message-flags-set! s2 uid2 flags1)
			       (bimap-sync-info-store! syncs synct syncf
				  mid1 #f flags1)
			       (mailbox-folder-select! s1 f1)))))))))))
   
   (define (synchronize-messages-new syncs::mailbox
	      synct::struct syncf::bstring
	      s1::mailbox f1::bstring
	      s2::mailbox f2::bstring)
      ;; (tprint "synchronize-messages-new")
      (mailbox-folder-select! s1 f1)
      (mailbox-folder-select! s2 f2)
      (let ((mids1 (mailbox-folder-mids-table s1 f1))
	    (mids2 (mailbox-folder-mids-table s2 f2))
	    (tf1 (mailbox-folder-flags s1)))
	 (hashtable-for-each mids1
	    (lambda (mid1 uid1)
	       (unless (or (hashtable-get synct mid1) (hashtable-get mids2 mid1))
		  ;; new in m2
		  (let* ((hd1 (bimap-mailbox-message-header s1 uid1))
			 (minfo (list uid1
				   (cons 'flags
				      (bimap-mailbox-message-flags s1 uid1 tf1))
				   (cons 'message-id mid1))))
		     (unless ((bimap-filter) s1 f1 minfo hd1)
			(let ((fdest ((bimap-folder-rewrite) s2 f2 mid1 hd1))
			      (content1 (mailbox-message s1 uid1))
			      (flags1 (bimap-mailbox-message-flags s1 uid1 tf1)))
			   (bimap-log-copy s1 f2 mid1 s2 f2 uid1)
			   (when (message-copy! s2 fdest content1 flags1)
			      (bimap-sync-info-store! syncs synct syncf
				 mid1 #f flags1)
			      (mailbox-folder-select! s1 f1))))))))))
   
   (with-trace 1 'sync-folders!
      (let ((f1 (make-mailbox-folder-name s1 f1))
	    (f2 (make-mailbox-folder-name s2 f2)))
	 (trace-item "s1=" s1)
	 (trace-item "f1=" f1)
	 (trace-item "s2=" s2)
	 (trace-item "f2=" f2)
	 (bimap-verb 1
	    " " (trace-color 3 s1 "://" f1) " <-> " (trace-color 4 s2 "://" f2)
	    "\n")
	 (with-handler
	    (lambda (e)
	       (exception-notify e)
	       (error "bimap" "Cannot synchronize folders"
		  (format "~a / ~a"
		     (trace-color 3 (format "~a://~a" s1 f1))
		     (trace-color 4 (format "~a://~a" s2 f2)))))
	    
	    ;; load the bimap sync-info
	    (let* ((syncf (bimap-sync-info-folder s1 f1 s2 f2))
		   (synct (bimap-sync-info-load s1 syncf)))
	       ;; update the messages flags
	       (unwind-protect
		  (begin
		     (synchronize-messages-insync s1 synct syncf s1 f1 s2 f2)
		     (synchronize-messages-outsync s1 synct syncf s1 f1 s2 f2)
		     (synchronize-messages-new s1 synct syncf s1 f1 s2 f2)
		     (synchronize-messages-new s1 synct syncf s2 f2 s1 f1))
		  (begin
		     (mailbox-folder-expunge! s1)
		     (mailbox-folder-expunge! s2))))))))
   
;*---------------------------------------------------------------------*/
;*    new-folder? ...                                                  */
;*    -------------------------------------------------------------    */
;*    A folder F@S1 is new with respect to S2 if it contains at        */
;*    least one non synchronized message.                              */
;*---------------------------------------------------------------------*/
(define (new-folder? s1::mailbox f1::bstring s2::mailbox f2::bstring)
   
   (define (subfolder-exists? s f)
      (any (lambda (f2)
	      (and (substring-at? f f2 0)
		   (>fx (string-length f2) (string-length f))
		   (char=? (string-ref f2 (string-length f))
		      (string-ref (mailbox-separator s) 0))))
	 (mailbox-folders s)))
   
   (with-trace 4 'new-folder?
      (trace-item "folder=" f1)
      (trace-item "subfolder-exists?=" (subfolder-exists? s1 f1))
      (or (subfolder-exists? s1 f1)
	  (let* ((syncf (bimap-sync-info-folder s1 f1 s2 f2))
		 (synct (bimap-sync-info-load s1 syncf)))
	     (>fx (hashtable-size synct) 0)))))

;*---------------------------------------------------------------------*/
;*    synchronize-servers-folders! ...                                 */
;*---------------------------------------------------------------------*/
(define (synchronize-servers-folders! s1 root1::bstring folders1::pair-nil
	   s2 root2::bstring folders2::pair-nil)
   
   (define (add/del-folder! s1::mailbox root1::bstring
	      s2::mailbox root2::bstring
	      f1::bstring f2::bstring)
      (let ((f (substring f1 (string-length root1) (string-length f1))))
	 (if (new-folder? s1 f1 s2 f2)
	     (let ((f2 (string-append root2 f)))
		(with-handler
		   (lambda (e)
		      (exception-notify e)
		      #f)
		   (begin
		      (folder-create!
			 s2 (make-folder-name f2
			       "." (mailbox-separator s2)) #t)
		      (synchronize-folders! s1 f1 s2 f2))))
	     (begin
		(when (bimap-delete-folders)
		   (with-handler
		      (lambda (e) #f)
		      (folder-delete! s1 f1)))
		#f))))
   
   (define (unroot folder root)
      (substring folder (string-length root) (string-length folder)))

   (let loop ((folders1 (sort string<=?
			   (map (lambda (f)
				   (make-folder-name f
				      (mailbox-separator s1) "."))
			      folders1)))
	      (folders2 (sort string<=?
			   (map (lambda (f)
				   (make-folder-name f
				      (mailbox-separator s2) "."))
			      folders2))))
      (cond
	 ((null? folders1)
	  (for-each (lambda (f)
		       (let ((nf (make-folder-name
				    f
				    (mailbox-separator s1)
				    (mailbox-separator s2))))
			  (add/del-folder! s2 root2 s1 root1 nf f)))
	     folders2))
	 ((null? folders2)
	  (for-each (lambda (f)
		       (let ((nf (make-folder-name
				    f
				    (mailbox-separator s2)
				    (mailbox-separator s1))))
			  (add/del-folder! s1 root1 s2 root2 nf f)))
	     folders1))
	 ((string=? (unroot (car folders1) root1)
	     (unroot (car folders2) root2))
	  (synchronize-folders! 
	     s1 (car folders1)
	     s2 (car folders2))
	  (loop (cdr folders1) (cdr folders2)))
	 ((string<? (unroot (car folders1) root1)
	     (unroot (car folders2) root2))
	  (let* ((f (car folders2))
		 (nf (make-folder-name
			f
			(mailbox-separator s1)
			(mailbox-separator s2))))
	     (let ((n (add/del-folder! s2 root2 s1 root1 nf f)))
		(loop folders1
		   (cdr folders2)))))
	 (else
	  (let* ((f (car folders1))
		 (nf (make-folder-name
			f
			(mailbox-separator s2)
			(mailbox-separator s1))))
	     (let ((n (add/del-folder! s1 root1 s2 root2 nf f)))
		(loop (cdr folders1) folders2)))))))

;*---------------------------------------------------------------------*/
;*    synchronize-servers! ...                                         */
;*---------------------------------------------------------------------*/
(define (synchronize-servers!
	   s1::mailbox root1::bstring
	   s2::mailbox root2::bstring)
   (bimap-verb 1 
      (trace-color 1 (format "~a://~a" s1 root1))
      " / "
      (trace-color 2 (format "~a://~a" s2 root2))
      " ["
      (let ((now (current-date)))
	 (format "~a:~a ~a~a~a"
	    (date-hour now)
	    (date-minute now)
	    (date-day now)
	    (month-aname (date-month now))
	    (date-year now)))
      "]\n")
   ;; lock
   (unless (bimap-force-lock) (bimap-lock! s1 s2))
   ;; synchronize
   (unwind-protect
      (begin
	 (let ((folders1 (filter (lambda (x) (synchronize-folder? s1 x root1))
			    (mailbox-folders s1)))
	       (folders2 (filter (lambda (x) (synchronize-folder? s2 x root2))
			    (mailbox-folders s2))))
	    ;; create root folders
	    (folder-create! s1 root1 #t)
	    (folder-create! s2 root2 #t)
	    ;; synchronize root folders
	    (synchronize-servers-folders! s1 root1 folders1 s2 root2 folders2)))
      ;; unlock
      (bimap-unlock! s1 s2)))

;*---------------------------------------------------------------------*/
;*    *synchronizations* ...                                           */
;*---------------------------------------------------------------------*/
(define *synchronizations* '())

;*---------------------------------------------------------------------*/
;*    synchronize-all! ...                                             */
;*---------------------------------------------------------------------*/
(define (synchronize-all!)
   ;; synchronize the servers
   (for-each (lambda (v)
		(let ((s1 (vector-ref v 0))
		      (f1 (vector-ref v 1))
		      (s2 (vector-ref v 2))
		      (f2 (vector-ref v 3)))
		   (cond
		      ((and (not f1) (not f2))
		       (synchronize-servers!
			  s1 (bimap-folder-inbox)
			  s2 (bimap-folder-inbox)))
		      ((not f1)
		       (synchronize-servers!
			  s1 (bimap-folder-inbox)
			  s2 f2))
		      (else
		       (synchronize-servers!
			  s1 f1
			  s2 (bimap-folder-inbox))))))
      (reverse! *synchronizations*)))

;*---------------------------------------------------------------------*/
;*    register-synchronize! ...                                        */
;*---------------------------------------------------------------------*/
(define (register-synchronize! sync1 sync2)
   (define (parse sync)
      (cond
	 ((mailbox? sync)
	  (values sync #f))
	 ((and sync (mailbox? (car sync)) (string? (cadr sync)))
	  (values (car sync) (cadr sync)))
	 (else
	  (error "bimap" "Illegal synchronization specification" sync))))
   (let ((s1 #f)
	 (f1 #f)
	 (s2 #f)
	 (f2 #f))
      (multiple-value-bind (s f)
	 (parse sync1)
	 (set! s1 s)
	 (set! f1 f))
      (multiple-value-bind (s f)
	 (parse sync2)
	 (set! s2 s)
	 (set! f2 f))
      (set! *synchronizations*
	 (cons (vector s1 f1 s2 f2) *synchronizations*))))

